#Build the Go server
FROM golang:1.14 AS go

WORKDIR /go/src/app

COPY ./server/ .

RUN make
# Binary exists at bin/server

#Build Vue.js SPA
FROM node:12 AS node
WORKDIR /home/node/app

COPY package*.json ./
RUN npm ci

COPY . .

RUN npm run build
#HTML now exists at dist/

FROM nginx:1.17
EXPOSE 8080

COPY --from=go /go/src/app/bin/server /bin/server
COPY --from=node /home/node/app/dist/ /usr/share/nginx/html

COPY deploy/nginx.conf /etc/nginx/conf.d/default.conf

COPY deploy/startup.sh /startup.sh
RUN chmod +x /startup.sh

ENTRYPOINT ["/startup.sh"]
