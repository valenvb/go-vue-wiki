image:
	docker build -t md-wiki:2019 .

run:
	docker run --rm -it -p 8080:8080 md-wiki:2019

build-run: image run
