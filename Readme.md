# Go Vue.js Wiki

## API Server

The API server (wriiten in go) is in the `server/src/` directory. It can be built by simply running `make` in the `server` directory, or run using `make run`. These produce a binary in the `./bin` folder. The server can also be compiled directly by running `go build -o server` in the `server/src` folder.

## Web App

The web app is a Vue.JS app. It can be built using `npm ci && npm run build`, or served for local development (without the API) with npm run serve. Unit test (written using Jest) can be run with `npm run test`.

## Docker Image

The included Dockerfile builds both the Go API server and the Vue.js SPA, and then produces an image based on Nginx that servers both the SPA and API. The included Makefile builds (and runs) the Docker image. `make` or `make image` will build the Docker image, which can then be run with `make run`. `make build-run` will rebuild and then run the image.

Addiitonally, GitLab CI builds and pushes this image to the Resgitry associated with this project: https://gitlab.com/valenvb/go-vue-wiki/container_registry
