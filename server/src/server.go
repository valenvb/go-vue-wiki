package main

import (
	"encoding/json"
	"fmt"
	"go-vue-wiki/wiki"
	"io/ioutil"
	"log"
	"net/http"
)

func handleQuery(w http.ResponseWriter, r *http.Request) {
	title := r.URL.Path[len("/articles/"):]

	switch r.Method {
	case http.MethodGet:

		if title == "" {
			articles := wiki.GetAllNames()
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(articles)
		} else {
			article, err := wiki.GetEntry(title)
			if err != nil {
				w.WriteHeader(http.StatusNotFound)
			} else {
				w.Header().Set("Content-Type", "text/html")
				fmt.Fprint(w, article.Content)
			}
		}
		break
	case http.MethodPut:

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			http.Error(w, err.Error(), 500)
		} else {
			content := string(body)

			_, updated, err := wiki.AddEntry(title, content)

			if err != nil {
				http.Error(w, err.Error(), 500)
				return
			}

			if updated {
				w.WriteHeader(http.StatusOK)
			} else {
				w.WriteHeader(http.StatusCreated)
			}
		}
		break
	default:
		w.WriteHeader(http.StatusNotImplemented)
		return
	}
}

func registerHandlers() {
	http.HandleFunc("/articles/", handleQuery)
}

func main() {
	fmt.Println("API server started")

	registerHandlers()
	log.Fatal(http.ListenAndServe(":9090", nil))
}
