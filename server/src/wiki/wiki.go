package wiki

import (
	"errors"
	"strings"
)

//Entry holds a single wiki article
type Entry struct {
	Name    string
	Content string
}

//wiki is a map of names => Entry's
var wiki = make(map[string]Entry)

//AddEntry adds a new entry to the wiki.
//Name must be a non-empty string, content can be any string value
func AddEntry(name string, content string) (*Entry, bool, error) {
	update := false
	//only operate on non-empty names
	name = strings.TrimSpace(name)
	if name == "" {
		return nil, false, errors.New("Name must not be empty")
	}

	//check if wiki is nil, initialize if it is
	if wiki == nil {
		wiki = make(map[string]Entry)
	}

	//will this be an update?
	_, update = wiki[name]
	// fmt.Printf("%s: %t\n", name, update)

	newEntry := Entry{Name: name, Content: content}
	wiki[name] = newEntry
	return &newEntry, update, nil
}

//GetEntry returns a single entry from a provided entry name
func GetEntry(name string) (*Entry, error) {

	entry, exists := wiki[name]

	if !exists {
		return nil, errors.New("Entry not found")
	}
	return &entry, nil

}

//GetAllNames returns an array of all wiki entry names
func GetAllNames() []string {

	names := make([]string, len(wiki))
	i := 0
	for name := range wiki {
		names[i] = name
		i++
	}

	return names
}
