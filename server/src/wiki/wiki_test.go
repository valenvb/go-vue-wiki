package wiki

import (
	"testing"
)

func TestCanAddEntry(t *testing.T) {
	name := "entry"
	content := "some text"

	result, updated, err := AddEntry(name, content)

	if err != nil || result == nil {
		t.Errorf("Expected adding an entry to succeed, returned %s", err)
	}

	if updated {
		t.Errorf("Expected not to have updated entry")
	}

	if result.Content != content {
		t.Errorf("Entry conent is not returned. Expected '%s' got %s", content, result.Content)
	}

	if entry, ok := wiki[name]; ok {
		if entry.Content != content {
			t.Errorf("Entry conent is not saved. Expected '%s' got %s", content, entry.Content)
		}
	} else {
		t.Errorf("Expected entry '%s' to exist. Got %t", name, ok)
	}

	result, updated, err = AddEntry(name, "other text")

	if !updated {
		t.Errorf("Expected to have updated existing entry")
	}

	if result.Content != "other text" {
		t.Errorf("Update did not reply with updated entry")
	}

	if wiki[name].Content != "other text" {
		t.Errorf("New content did not get saved")
	}

}

func TestErrorIfAddWithEmptyName(t *testing.T) {
	_, _, err := AddEntry("", "blah")

	if err == nil {
		t.Errorf("Expected error when adding entry with no name. Got nil")
	}
}

func TestGetEntry(t *testing.T) {
	//setup the wiki
	name := "entry-1"
	content := "Some Content"

	wiki = map[string]Entry{"entry": Entry{Name: name, Content: content}}

	result, err := GetEntry("entry")
	if err != nil {
		t.Errorf("Expected not to get an error. Got %s", err)
	}

	if result.Name != name || result.Content != content {
		t.Errorf("Enexpected Response. Got %s", result)
	}

}

func TestGetEntryNoData(t *testing.T) {
	//clear the wiki
	wiki = make(map[string]Entry)

	_, err := GetEntry("entry")
	if err == nil {
		t.Errorf("Expected to get an error. Got %s", err)
	}

}
func TestGetAllNames(t *testing.T) {
	//reset the wiki
	wiki = make(map[string]Entry)
	//on first run it should be an empty array
	result := GetAllNames()
	if len(result) > 0 {
		t.Errorf("Expected empty array, got %s", result)
	}

	names := []string{"1", "test", "a few"}

	for _, name := range names {
		AddEntry(name, "")
	}

	result = GetAllNames()
	if !equal(result, names) {
		t.Errorf("Expected names to match %s, got %s", names, result)
	}
}

func equal(a []string, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for _, v := range a {
		if !sliceContains(&b, v) {
			return false
		}
	}
	return true
}

func sliceContains(s *[]string, str string) bool {
	for _, v := range *s {
		if v == str {
			return true
		}
	}
	return false
}
