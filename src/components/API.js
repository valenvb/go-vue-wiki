import axios from "axios";
axios.defaults.baseURL = "/articles";

export async function getArticle(article) {
  //console.log("API Fetch", article);
  try {
    let result = await axios.get(article);
    if (result.status === 200) {
      return result.data;
    } else {
      return false;
    }
  } catch (err) {
    if (err.response.status === 404) {
      return false;
    }
  }
}

export async function getAllArticles() {
  //console.log("API Fetch List");
  try {
    let result = await axios.get();
    if (result.status === 200) {
      return result.data;
    } else {
      return [];
    }
  } catch (err) {
    //console.log(err);
    return [];
  }
}

export async function saveArticle(name, content) {
  //console.log("save: ", name, content);
  try {
    await axios.put(name, content);
    return true;
  } catch (err) {
    //console.log(err);
    return false;
  }
}
