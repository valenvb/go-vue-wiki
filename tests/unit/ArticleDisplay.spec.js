import { mount } from "@vue/test-utils";
import ArticleDisplay from "@/components/ArticleDisplay.vue";
import VueMarkdown from "vue-markdown";

describe("ArticleDisplay", () => {
  it("renders supplied Markdown content", () => {
    const content = "# Some Content \n ~~fun~~";
    const wrapper = mount(ArticleDisplay, {
      propsData: { content },
      stubs: {
        "vue-markdown": VueMarkdown
      }
    });

    expect(wrapper.find(".content").exists()).toBe(true);
    expect(wrapper.find("div.content").html()).toMatchInlineSnapshot(`
      <div class="content">
        <div>
          <h1>Some Content</h1>
          <p><s>fun</s></p>
        </div>
      </div>
    `);
  });
});
