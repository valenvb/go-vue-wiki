import { shallowMount } from "@vue/test-utils";
import ArticleEditor from "@/components/ArticleEditor.vue";

describe("Article Editor", () => {
  it("Displays current article content in the textarea", () => {
    const content = "some content";
    const $route = { params: { name: "test" } };
    const wrapper = shallowMount(ArticleEditor, { propsData: { content }, mocks: {$route}, stubs:["router-link"]});
    const textarea = wrapper.find("textarea");

    expect(textarea.exists()).toBe(true);
    expect(textarea.element.value).toBe(content);
  });
});
