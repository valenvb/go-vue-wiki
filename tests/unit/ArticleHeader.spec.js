import { shallowMount } from "@vue/test-utils";
import ArticleHeader from "@/components/ArticleHeader.vue";

describe("Article Header", () => {
  const name = "entry";
  const $route = { params: { name } };

  it("Displays name from route in Title Case", () => {
    const wrapper = shallowMount(ArticleHeader, { mocks: { $route }, stubs:["router-link"]});

    expect(wrapper.find("h1").text()).toBe(name);
  });

  it("has an edit button linking to /edit/:name", () => {
    const wrapper = shallowMount(ArticleHeader, { mocks: { $route }, stubs:["router-link"] });

    const edit_button = wrapper.find("router-link-stub.btn");
    expect(edit_button.exists()).toBe(true);
    expect(edit_button.text()).toBe("Edit");
    expect(edit_button.attributes("to")).toBe("/edit/" + name);
  });
});
