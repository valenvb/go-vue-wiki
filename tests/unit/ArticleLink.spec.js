import { shallowMount } from "@vue/test-utils";
import ArticleLink from "@/components/ArticleLink.vue";

describe("AritcleLink", () => {
  it("displays a link to an article passed from a prop", () => {
    const article = "article";
    const wrapper = shallowMount(ArticleLink, {
      propsData: { article },
      stubs: ["router-link"]
    });
    expect(wrapper.text()).toBe(article);
    expect(wrapper.find("router-link-stub").attributes("to")).toBe("/" + article);
  });
});
