import { shallowMount, mount } from "@vue/test-utils";
import ArticleList from "@/components/ArticleList.vue";
let getAll = require("@/components/API").getAllArticles;

//const getAllArticles = jest.genMockFromModule("@/components/API").getAllArticles
jest.mock("@/components/API");

describe("ArticleList", () => {
  it("has as many ArticleLinks as articles", async () => {
    const mock_articles = ["one", "two"];
    getAll.mockResolvedValue(mock_articles);
    const wrapper = shallowMount(ArticleList);
    await wrapper.vm.$nextTick();
    const links = wrapper.findAll("article-link-stub");
    expect(links.exists()).toBe(true);
    expect(links.length).toBe(mock_articles.length);
  });

  it("passes each article to an ArticleLink", () => {
    const mock_articles = ["one", "two"];
    getAll.mockResolvedValue(mock_articles);
    const wrapper = mount(ArticleList, {
      stubs: ["router-link"]
    });
    const links = wrapper.findAll("article-link");
    for (let i = 0; i < links.length; i++) {
      const link = links.at(i);
      expect(link.attributes("to")).toBe(mock_articles[i]);
    }
  });
});
